const pluginWebc = require("@11ty/eleventy-plugin-webc");
const bundlerPlugin = require("@11ty/eleventy-plugin-bundle");
const { eleventyImagePlugin } = require("@11ty/eleventy-img");
const sass = require("sass");
const path = require("node:path");
const pluginRss = require("@11ty/eleventy-plugin-rss");

module.exports = (config) => {
	
	// WebC
	config.addPlugin(pluginWebc, {
		components: [
			"src/component/*.webc",
			"npm:@11ty/eleventy-img/*.webc",// Add as a global WebC component
		]
	});
	
	config.addPlugin(bundlerPlugin, {
		// Folder (in the output directory) bundle files will write to:
		toFileDirectory: "asset",
		
		// Extra bundle names ("css", "js", "html" are guaranteed)
		//bundles: ['css'],
		
		// Array of async-friendly callbacks to transform bundle content.
		// Works with getBundle and getBundleFileUrl
		transforms: [],
		
		// Array of bundle names eligible for duplicate bundle hoisting
		hoistDuplicateBundlesFor: [], // e.g. ["css", "js"]
	});
	
	// Image plugin
	config.addPlugin(eleventyImagePlugin, {
		formats: ["svg", "avif", "webp"],
		svgShortCircuit: true,
		filenameFormat: function (id, src, width, format, options) {
			const extension = path.extname(src);
			const name = path.basename(src, extension);
			
			if ( format === "svg") {
				return `${name}.${format}`;
			} else {
				return `${name}-${width}w.${format}`;
			}
		},
		urlPath: "/ricettario/asset/img/",// A path-prefix-esque directory for the <img src> attribute.
		outputDir: "./dist/asset/img/",// Where to write the new images to disk. Project-relative path to the output image directory.
		defaultAttributes: {
			loading: "lazy",
			decoding: "async",
		},
	});
	
	// Atom feed plugin
	config.addPlugin(pluginRss);
	
	config.addPassthroughCopy({ static: './' });// Copy `static/` content to `dist/`
	config.addPassthroughCopy({'src/img/favicon': './asset/img/favicon'});
	//config.addPassthroughCopy('src/fonts');
	//config.addPassthroughCopy('src/admin');

	config.addTemplateFormats("scss");

	// Creates the extension for use
	config.addExtension("scss", {
		outputFileExtension: "css", // optional, default: "html"
		
		compileOptions: {
			permalink: function(contents, inputPath) {
				return (data) => {
					return `asset/${data.page.fileSlug}.css`;
					// Return a string to override: you’ll want to use `data.page`
					// Or `return;` (return undefined) to fallback to default behavior
				}
			}
		},
		
		// `compile` is called once per .scss file in the input directory
		compile: async function (inputContent, inputPath) {
			let parsed = path.parse(inputPath);
			// You can pass in both the file’s inputPath and the Eleventy includes folder
			// to provide a set of directories to look for when using Sass’ @use, @forward,
			// and @import features.
			let result = sass.compileString(inputContent, {
				loadPaths: [
					parsed.dir || ".",
					this.config.dir.includes
				]
			});
			// This is the render function, `data` is the full data cascade
			return async (data) => {
				return result.css;
			};
		}
	});

	/* Collections */
	/* 
	config.addCollection('recipes', collection => {
		return [...collection.getFilteredByGlob('./recipes/*.md')];
	});
	
	config.addCollection('tagList', collection => {
		const tagsSet = new Set();
		collection.getAll().forEach(item => {
			if (!item.data.tags) return;
			item.data.tags
				.filter(tag => !['recipes'].includes(tag))
				.forEach(tag => tagsSet.add(tag));
		});
		return Array.from(tagsSet).sort((first, second) => {
			const firstStartingLetter = first.replace(emojiRegex, '').trim()[0].toLowerCase();
			const secondStartingLetter = second.replace(emojiRegex, '').trim()[0].toLowerCase();

			if (firstStartingLetter < secondStartingLetter) { return -1; }
			if (firstStartingLetter > secondStartingLetter) { return 1; }
			return 0;
		});
	});
	 */
	
	/* Filters */
	/* 
	config.addFilter('console', function(value) {
		return util.inspect(value);
	});
	config.addFilter('noEmoji', function(value) {
		return value.replace(emojiRegex, '').trim();
	});

	config.addFilter('onlyEmoji', function(value) {
		let match = value.match(emojiRegex);
		// If the string doesn't contain any emoji, instead we output the first letter wrapped in some custom styles
		if (!match) {
			match = `<span class="c-card__tag-first-letter">${value.charAt(0)}</span>`
		}
		return Array.isArray(match) ? match.join('') : match;
	});

	config.addFilter('limit', (arr, limit) => arr.slice(0, limit));

	config.addFilter('lowercase', function(value) {
		return value.toLowerCase();
	});

	// This workaround is needed so we can transform it back into an array with Alpine (we can't split on "," as it can be included within the items)
	config.addFilter('arrayToString', function(value) {
		return encodeURI(value.join('£'));
	});
	 */

	/* Shortcodes */
	/* 
	const imageShortcode = async (src, className, alt, sizes) => {
		let metadata = await Image(src.includes('http') ? src : `./src/${src}`, {
			widths: [600, 1500, 3000],
			formats: ['webp', 'jpeg'],
			outputDir: './_site/img/recipes',
			urlPath: '/img/recipes/'
		});
	
		let imageAttributes = {
			class: className,
			alt,
			sizes,
			loading: "lazy",
			decoding: "async"
		};
	
		return Image.generateHTML(metadata, imageAttributes);
	}
	 */
	//config.addNunjucksAsyncShortcode('recipeimage', imageShortcode);
	//config.addShortcode('year', () => `${new Date().getFullYear()}`);
	config.watchIgnores.add("src/data/recipeEssential.json");
	
	return {
		dir: {
			input: '.',
			output: 'dist',
			includes: 'src/layout',
			//layouts: '',
			data: 'src/data',
		},
		pathPrefix: '/ricettario/',
		templateFormats: ["md", "njk"],
		passthroughFileCopy: true,
	}
};
