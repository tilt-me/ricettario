# Ricettario

Repo for a quite simple recipe blog, built with 11ty.


## Install

Run `npm i` to install dependencies.


## Develop

1. Just run `npm run dev` to start a local dev server at `http://localhost:8080`
2. Edit files and see the results in the browser
3. Happy coding 🎉


### Tone of voice

In recipes, use second-person singular to refer to your reader.


## Build

Every commit to master will trigger a build to Gitlab Pages 🚀
