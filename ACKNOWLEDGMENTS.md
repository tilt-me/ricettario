# ACKNOWLEDGMENTS 🙏

This website is coded with a lot of passion and thanks to the following resources.


## 11ty Rocks!

- [Going Beyond Static Sites With Eleventy > Flexible Output](https://11ty.rocks/posts/going-beyond-static-with-eleventy/#flexible-output)
