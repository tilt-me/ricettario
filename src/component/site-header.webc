<script webc:setup>
	function hasLink(currentCourse) {
		return collections[currentCourse];
	}
</script>

<header class="header" webc:root="override">
	<div class="container is-max-desktop">
		<nav class="navbar" role="navigation" aria-label="main navigation">
			<div class="navbar-brand">
				<a class="navbar-item" href="/ricettario/">
					<img
						webc:is="eleventy-image"
						class="logo"
						loading="eager"
						src="./src/img/1f37d.svg"
						alt=":plate:"
					/>
				</a>
				
				<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navMenu">
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
				</a>
			</div>
			<div class="navbar-menu" id="navMenu">
				<div class="navbar-start">
					<div class="navbar-item has-dropdown">
						<a class="navbar-link">
							Ricette
						</a>
						<div class="navbar-dropdown">
							<a webc:if="hasLink('colazione')" class="navbar-item" href="/ricettario/portate/colazione/">Colazione</a>
							<a webc:if="hasLink('antipasti')" class="navbar-item" href="/ricettario/portate/antipasti/">Antipasti</a>
							<a webc:if="hasLink('primi')" class="navbar-item" href="/ricettario/portate/primi/">Primi</a>
							<a webc:if="hasLink('secondi')" class="navbar-item" href="/ricettario/portate/secondi/">Secondi</a>
							<a webc:if="hasLink('contorni')" class="navbar-item" href="/ricettario/portate/contorni/">Contorni</a>
							<a webc:if="hasLink('salse')" class="navbar-item" href="/ricettario/portate/salse/">Salse</a>
							<a webc:if="hasLink('piatti unici')" class="navbar-item" href="/ricettario/portate/piatti unici/">Piatti unici</a>
							<a webc:if="hasLink('dolci')" class="navbar-item" href="/ricettario/portate/dolci/">Dolci</a>
							<a webc:if="hasLink('bevande')" class="navbar-item" href="/ricettario/portate/bevande/">Bevande</a>
							<!---
							<hr class="navbar-divider">
							<a class="navbar-item">Carne</a>
							<a class="navbar-item">Pesce</a>
							<a class="navbar-item">Verdura</a>
							<a class="navbar-item">Alcol</a>
							--->
						</div>
					</div>
				</div>
				<div class="navbar-end">
					<a class="navbar-item" href="/ricettario/info/">Info</a>
				</div>
			</div>
		</nav>
	</div>
</header>

<style webc:type="11ty" 11ty:type="scss">
	@import "../sass/custom.scss";// only variables, don't output anything
	
	.header {
		background-color: $tablecloth-fore;
		background-image:
			repeating-linear-gradient(-45deg, $tablecloth-back, $tablecloth-back 10px, $tablecloth-fore 10px, $tablecloth-fore 20px),
			repeating-linear-gradient(45deg, $tablecloth-back, $tablecloth-back 10px, $tablecloth-fore 10px, $tablecloth-fore 20px)
		;
		position: sticky;
		top: 0;
		z-index: 1;
		
		@include desktop {
			.container > .navbar .navbar-brand {
				margin-left: 0;
			}
			.container > .navbar .navbar-menu {
				margin-right: 0;
			}
		}
		
		.navbar-item img {
			max-height: none;
		}
	}
	
	.navbar-brand .navbar-item {
		padding-inline: 1rem;
		
		@include desktop {
			padding-inline: .75rem;
		}
	}
	
	.logo {
		width: 3rem;
	}
	
	.navbar-burger {
		height: 4rem;
		//margin-inline-end: 1rem;
		width: 4rem;
		
		span {
			height: 2px;
			left: calc(50% - 12px);
			width: 24px;
		}
	}
	
	.navbar-link:not(.is-arrowless) {
		padding-right: 3rem;
		
		@include desktop {
			padding-right: 2.5em;
		}
		
		&::after {
			right: 1.7rem;
			
			@include desktop {
				right: 1.125em;
			}
		}
	}
</style>

<script>
	// Get all "navbar-burger" elements
	const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
	// Add a click event on each of them
	$navbarBurgers.forEach( el => {
		el.addEventListener('click', () => {
			// Get the target from the "data-target" attribute
			const target = el.dataset.target;
			const $target = document.getElementById(target);
			
			// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
			el.classList.toggle('is-active');
			$target.classList.toggle('is-active');
		});
	});
	
	// Get all "navbar-item has-dropdown" elements
	const $navbarDropdowns = Array.prototype.slice.call(document.querySelectorAll('.navbar-item.has-dropdown'), 0);
	// Add a click event on each of them
	$navbarDropdowns.forEach( el => {
		el.addEventListener('click', () => {
			// Toggle the "is-active" class
			el.classList.toggle('is-active');
		});
	});
</script>
