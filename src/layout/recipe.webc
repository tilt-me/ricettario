---
layout: default.webc
---

<script webc:setup>
	function isValidTag(currentTag) {
		return (currentTag !== 'recipe');
	}
	
	function isCourse(currentTag) {
		return courseList.includes(currentTag);
	}
	/*
	function isIngredient(currentTag) {
		return ingredientList.includes(currentTag);
	}
	*/
	function formatDate(rawDate) {
		return new Date(rawDate).toLocaleDateString('en-GB');
	}
</script>

<section class="section">
	<div class="container is-max-desktop">
		<article>
			<div class="columns">
				<div class="column">
					<h1 class="title" @text="title"></h1>
					<div class="subtitle" webc:if="excerpt" @text="excerpt"></div>
				</div>
				<div class="column">
					<div webc:if="cover && cover.coverUrl" class="is-relative">
						<img
							webc:is="eleventy-image"
							:src="cover.coverUrl.replace('/ricettario/', './static/')"
							:alt="title"
							class="thumb-image"
						/>
						<button class="btn-maximize button is-primary">
							<span class="icon">
								<img
									webc:is="eleventy-image"
									src="./src/img/1f441.svg"
									alt="+"
								/>
							</span>
						</button>
						<dialog class="dialog-image">
							<img
								webc:is="eleventy-image"
								:src="cover.coverUrl.replace('/ricettario/', './static/')"
								:alt="title"
								class="full-image"
							/>
							<form method="dialog">
								<button class="btn-close button is-primary">
									<span class="icon">
										<img
											webc:is="eleventy-image"
											class="img-close"
											src="./src/img/2716.svg"
											alt="✕"
										/>
									</span>
								</button>
							</form>
						</dialog>
					</div>
				</div>
			</div>
			
			<div class="level has-background-light has-text-centered py-2">
				<div class="level-item" webc:if="card.servings">
					<div>
						<p class="heading">Dosi per</p>
						<p class="subtitle is-3" @text="card.servings"></p>
					</div>
				</div>
				<div class="level-item" webc:if="card.procedureDuration">
					<div>
						<p class="heading">Tempo di preparazione</p>
						<p class="subtitle is-3" @text="card.procedureDuration"></p>
					</div>
				</div>
				<div class="level-item" webc:if="card.cookingDuration">
					<div>
						<p class="heading">Tempo di cottura</p>
						<p class="subtitle is-3" @text="card.cookingDuration"></p>
					</div>
				</div>
				<div class="level-item" webc:if="card.difficulty">
					<div>
						<p class="heading">Difficoltà</p>
						<p class="subtitle is-3" @text="card.difficulty"></p>
					</div>
				</div>
			</div>
			
			<div class="columns">
				<div webc:if="ingredients" class="column">
					<div class="block">
						<h2 class="heading">Ingredienti</h2>
						<div class="content">
							<ul class="ingredient-list">
								<li webc:for="ingredient of ingredients">
									<label class="checkbox">
										<input type="checkbox" />
										&nbsp;
										<span webc:if="ingredient.amount > 0" @text="ingredient.amount"></span>
										<span webc:if="ingredient.unit" @text="ingredient.unit"></span>
										<span @text="ingredient.name"></span>
									</label>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div webc:if="tools" class="column">
					<div class="block">
						<h2 class="heading">Utensili</h2>
						<div class="content">
							<ul>
								<li webc:for="tool of tools" @text="tool"></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			
			<hr />
			
			<div class="block">
				<h2 class="heading">Preparazione</h2>
				<div @raw="content" class="content"></div>
			</div>
			
			<div class="block" webc:if="preservation">
				<h2 class="heading">Conservazione</h2>
				<div @raw="preservation" class="content"></div>
			</div>
			
			<section class="message is-primary">
				<div class="message-body">
					<div class="columns">
					
						<div class="column is-half has-text-centered-mobile">
							Ricetta catalogata sotto:
							<br webc:if="!!revisionDate" />
							<ul class="tag-list">
								<template webc:for="tag of tags" webc:nokeep>
									<li webc:if="isCourse(tag)">
										<a :href="`/ricettario/portate/${tag}/`">
											<span class="tag is-warning" @text="tag"></span>
										</a>
									</li>
									<li webc:elseif="isValidTag(tag)">
										<a :href="`/ricettario/ingredienti/${tag}/`">
											<span class="tag is-warning is-light" @text="tag"></span>
										</a>
									</li>
								</template>
							</ul>
						</div>
					
						<div class="column is-half has-text-centered-mobile has-text-right">
							<div>
								Proposta
								<template webc:if="authors" webc:nokeep>
									da
									<ul class="author-list" webc:for="author of authors">
										<li><span class="tag is-info" @text="author"></span></li>
									</ul>
								</template>
								in data <time :datetime="new Date(publicationDate).toISOString()" @text="formatDate(publicationDate)"></time>
							</div>
							<div webc:if="!!revisionDate">
								Ultima revisione <time :datetime="new Date(revisionDate).toISOString()" @text="formatDate(revisionDate)"></time>
							</div>
						</div>
					
					</div>
				</div>
			</section>
			
			<details class="block" webc:if="sources && !!sources.length">
				<summary class="heading">Fonti di ispirazione</summary>
				<div class="content has-background-light py-3">
					<ul class="mt-0">
						<li webc:for="sourceItem of sources"><small><a :href="sourceItem.sourceUrl" @text="sourceItem.sourceName"></a></small></li>
					</ul>
				</div>
			</details>
		</article>
		<related :relatedrecipe="relatedRecipe"></related><!-- please use lowercase attributes -->
	</div>
</section>

<style webc:type="11ty" 11ty:type="scss">
	@import "../sass/custom.scss";// only variables, don't output anything
	
	html.has-modal {
		overflow: hidden;
	}
	
	.thumb-image {
		border-radius: 1em;
		display: block;
		margin: 0 auto;
		max-height: 33vh;
		width: auto;
		
		@include tablet {
			margin: 0 0 0 auto;
		}
	}
	
	.btn-maximize {
		bottom: 1em;
		position: absolute;
		right: 1em;
	}
	
	.dialog-image {
		
		&::backdrop {
			background-color: $tablecloth-fore;
			background-image:
				repeating-linear-gradient(-45deg, $tablecloth-back, $tablecloth-back 15px, $tablecloth-fore 15px, $tablecloth-fore 30px),
				repeating-linear-gradient(45deg, $tablecloth-back, $tablecloth-back 15px, $tablecloth-fore 15px, $tablecloth-fore 30px)
			;
		}
		
		> picture {
			display: block;
			line-height: 0;
		}
	}
	
	.full-image {
		max-height: calc(85vh - 2rem);
		object-fit: contain;
	}
	
	.btn-close {
		position: absolute;
		right: 1em;
		top: 1em;
	}
	
	.img-close {
		filter: brightness(0) invert(100%);
	}
	
	.content .ingredient-list {
		list-style: none;
		margin-left: 1em;
		
		[type="checkbox"] {
			accent-color: $primary;
			//transform: scale(1.25);
		}
	}
	
	.tag-list {
		display: inline-flex;
		
		li {
			margin: 0 .5em .5em 0;
			
			&:last-child {
				margin-inline-end: 0;
			}
		}
	}
	
	.author-list {
		display: inline-flex;
		
		li {
			margin: 0 .25em;
		}
	}
	
	details {
		
		.heading {
			cursor: pointer;
			display: list-item;
		}
		
		.content {
			box-shadow: 1px 1px 2px $grey;
		}
	}
</style>

<script>
	const btnMaximize = document.getElementsByClassName("btn-maximize")[0];
	const dialogImage = document.getElementsByClassName("dialog-image")[0];
	const pageRoot = document.getElementsByTagName('html')[0];
	
	btnMaximize?.addEventListener("click", () => {
		dialogImage.showModal();
		pageRoot.classList.add('has-modal');
	});
	
	dialogImage?.addEventListener("close", (event) => {
		pageRoot.classList.remove('has-modal');
	});
</script>
