---
title: Besciamella
excerpt: ""
publicationDate: 2021-11-07
authors:
  - B
card:
  difficulty: facile
  servings: ""
tags:
  - salse
ingredients:
  - name: latte
    amount: 500
    unit: ml
  - name: burro
    amount: 40
    unit: g
  - name: farina
    amount: 40
    unit: g
  - name: sale
    amount: 0
    unit: null
  - name: noce moscata
    amount: 0
    unit: null
tools:
  - pentolino con manico marrone
  - frusta
---
Far sciogliere il burro nel pentolino, poi aggiungere la farina mescolando con la frusta e aggiungere pian piano il latte, sempre mescolando per evitare che si formino grumi. Aggiungere sale e noce moscata grattugiata.

Appena comincia a bollire spegnere subito il fuoco.
