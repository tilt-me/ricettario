---
title: Risotto ai funghi
publicationDate: 2022-11-20
authors:
  - T
cover:
  coverUrl: /ricettario/uploads/risotto-funghi.webp
card:
  servings: "2"
  cookingDuration: 20 minuti
  procedureDuration: 25 minuti
  difficulty: media
tags:
  - primi
  - riso
  - verdure
ingredients:
  - amount: 25
    name: burro
    unit: g
  - amount: 0
    name: ½ porro (o cipolla)
  - amount: 200
    name: funghi prataioli (noti anche come champignon)
    unit: g
  - amount: 200
    name: riso carnaroli
    unit: g
  - amount: 10
    name: vino bianco
    unit: ml
  - amount: 700
    name: brodo vegetale
    unit: ml
tools:
  - pentolino antiaderente (quello con due manici)
  - pentolino per brodo, con coperchio
  - colino, per filtrare il brodo
  - coppino
preservation: Il risotto non si conserva granché bene, meglio mangiarlo tutto appena fatto.
---
Taglia finissimo il porro (o la cipolla) e fallo soffriggere nel burro per una decina di minuti.

Nel frattempo prepara il brodo (per una persona bastano 500ml) e taglia i funghi.

Quando il soffritto è pronto (porro o cipolla sono dorati) aggiungi i funghi, salali e metti anche il riso a tostare.

Sfuma poi con vino bianco.

Sfumato il vino, comincia a contare il tempo per cuocere il riso, che coprirai con mestoli di brodo, aggiungendone altri a mano a mano che si asciuga.

Quando il brodo è terminato il riso dovrebbe essere cotto: continua a farlo asciugare fino al punto che più ti aggrada, impiatta e mangia.
