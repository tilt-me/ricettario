---
title: Fusilli al salmone
excerpt: senza panna!
publicationDate: 2024-09-08
revisionDate: 2024-09-08
sources:
  - sourceUrl: https://ricette.giallozafferano.it/Farfalle-al-salmone-fresco.html
    sourceName: Giallo Zafferano
authors:
  - T
card:
  servings: "2"
  difficulty: facile
tags:
  - primi
  - pasta
  - pesce
ingredients:
  - amount: 125
    name: salmone fresco
    unit: g
  - amount: 0
    name: cipolla
  - amount: 0
    name: olio EVO
  - amount: 10
    name: vino bianco
    unit: ml
  - amount: 160
    name: fusilli
    unit: g
  - amount: 0
    name: sale grosso
  - amount: 0
    name: ½ vasetto di yogurt al naturale (senza zucchero)
    unit: ""
  - amount: 0
    name: prezzemolo
tools:
  - pentola (per la pasta)
  - scolapasta
  - padella (per cuocere il salmone)
  - tagliere bianco
---
Comincia a far bollire l'acqua per la pasta.

Taglia il salmone a tocchetti di 2-3cm. Taglia la cipolla finemente e prepara il soffritto in padella: olio e cipolla; lascia imbiondire la cipolla e poi aggiungi anche il salmone; cuoci per qualche minuto girando delicatamente; sfuma con vino bianco prima che il salmone diventi troppo secco.

Nel frattempo la pasta dovrebbe essere cotta: scolala nel condimento per saltarla un attimo.

A fuoco spento aggiungi in padella lo yogurt e un po' di prezzemolo sminuzzato e impiatta.
