---
title: Pasta al forno coi broccoli
publicationDate: 2022-11-07
authors:
  - B
card:
  servings: 4 persone
  difficulty: media
  cookingDuration: ""
tags:
  - primi
  - pasta
  - verdure
ingredients:
  - amount: 1
    name: broccolo grande
  - amount: 125
    name: ricotta
    unit: g
  - amount: 1
    name: mozzarella
  - amount: 400
    name: maccheroni
    unit: g
  - amount: 1
    name: spicchio d'aglio
  - amount: 0
    name: peperoncino
  - amount: 0
    name: olio d'oliva
  - amount: 0
    name: parmigiano grattugiato
tools:
  - pirofila quadrata blu grande
  - padella grande (per soffritto)
  - pentola (per bollire broccolo e pasta)
preservation: Conservare in frigo per qualche giorno massimo e scaldare in
  padella antiaderente con un coperchio (e un tocco di burro).
---
Lessare il broccolo.

In una padella grande fare soffritto con aglio e peperoncino, passare il broccolo in padella, schiacciarlo e mettere sale.

Aggiungere sempre in padella ricotta e un po' di acqua di cottura del broccolo.

Cuocere la pasta e poi buttarla in padella in modo da condirla con broccolo.

Mettere olio in taglia e mettere un primo strato di pasta, poi formaggio grattugiato e mozzarella; poi fare un secondo strato allo stesso modo.

Infornare per 20 minuti a 180° statico, più 5 minuti in modalità grill superiore (per far venire crosticina).
