---
title: Frullato fricchettone
excerpt: Senza latte, senza zucchero (aggiunto)!
publicationDate: 2020-10-17
authors:
  - T
cover:
  coverUrl: /ricettario/uploads/frullato.webp
  caption: ""
card:
  servings: 1 persona
  procedureDuration: 5 minuti
  cookingDuration: ""
  difficulty: facile
tags:
  - bevande
intro: "*Senza latte, senza zucchero (aggiunto)!*"
ingredients:
  - name: banana
    amount: 1
  - name: bevanda di mandorla e/o riso
    amount: 250
    unit: ml
  - name: cucchiaino di cannella
    amount: 1
tools:
  - frullatore ad immersione
preservation: Ma bevilo subito, cosa stai a conservare?!
---
Metti la bevanda di riso e/o mandorla nel frullatore.

Taglia a rondelle la banana e mettila nel frullatore.

Aggiungi la cannella e frulla tutto!

A seconda di quanta era grossa la banana verrà più o meno denso, valuta tu come lo preferisci :wink:
