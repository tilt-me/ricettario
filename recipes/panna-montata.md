---
title: Panna montata
excerpt: yum
publicationDate: 2021-05-05
sources: []
authors: ""
card:
  difficulty: facile
  servings: 4 porzioni
tags:
  - dolci
ingredients:
  - name: panna fresca
    amount: 250
    unit: ml
  - name: zucchero a velo
    amount: 25
    unit: g
tools:
  - ciotola (beige)
  - frusta elettrica
---
Panna liquida in ciotola, montala con frusta e poi aggiungi un decimo di zucchero a velo (250ml di panna ➜ 25g di zucchero).
