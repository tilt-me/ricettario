---
title: Arrosto di vitello
publicationDate: 2022-01-16
authors:
  - B
card:
  difficulty: media
tags:
  - secondi
  - carne
ingredients:
  - amount: 1
    name: girello di vitello o polpa di vitello
  - amount: 0
    name: olio
  - amount: 1
    name: aglio
    unit: null
  - amount: 0
    name: aglio in polvere
  - amount: 0
    name: rosmarino
  - amount: 0
    name: vino bianco
    unit: null
  - amount: 0
    name: sale
  - amount: 0
    name: origano
  - amount: 2
    name: carote
tools:
  - pentolino alto
  - coperchio
---
Fai il soffritto con olio, spicchio d'aglio e rosmarino.

Aggiungi la carne e fai rosolare bene tutti i lati, in modo da sigillarla e prendere colore.

Aggiungi il vino bianco e fallo sfumare.

Aggiungi due bicchieri d'acqua circa, un paio di carote a tocchi e tieni sul fuoco con il coperchio per un'oretta; dopodiché lo tiri fuori un attimo per tagliarlo a fettine e lo rimetti in pentola. Aggiungi sale, aglio in polvere e origano, tieni sul fuoco un quarto d'ora circa, fino a far stringere l'acqua di cottura.
