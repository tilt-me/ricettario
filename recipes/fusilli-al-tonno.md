---
title: Fusilli al tonno
excerpt: col sugo di pomodoro!
publicationDate: 2024-03-22
sources:
  - sourceUrl: https://ricette.giallozafferano.it/Mezze-maniche-al-tonno.html
    sourceName: GialloZafferano
authors:
  - T
card:
  cookingDuration: 60 minuti
  difficulty: facile
  procedureDuration: 30 minuti
tags:
  - primi
  - pesce
  - pasta
ingredients:
  - amount: 1
    name: spicchio d'aglio
  - amount: 1
    name: acciuga sott'olio
  - amount: 1
    name: peperoncino
  - amount: 1
    name: scatoletta di tonno al naturale
  - amount: 1
    name: cucchiaio di vino bianco
  - amount: 700
    name: passata di pomodoro
    unit: ml
  - amount: 0
    name: olio EVO
  - amount: 80
    name: fusilli
    unit: g
tools:
  - pentola (per bollire la pasta)
  - padella antiaderente alta (per cuocere il sugo)
  - coperchio della padella (per evitare schizzi di sugo)
  - scolapasta
preservation: >
  Ovviamente ti avanzerà del sugo che puoi conservare in frigo per alcuni
  giorni, o anche congelare.
---
Prepara il soffritto: un cucchiaio d'olio in padella, aggiungi uno spicchio d'aglio; taglia il peperoncino a metà, rimuovi il picciolo e i semini interni; taglialo a listarelle e mettilo da parte.

Metti un acciuga nel soffritto, e quando è sciolta aggiungi anche il peperoncino.

Metti il tonno e sfuma con un cucchiaio di vino bianco. Quando il vino si è ritirato, aggiungi la passata di pomodoro e sala, aggiungendo un po' di acqua del rubinetto (sfrutta il contenitore vuoto della passata, così ne prendi le ultime gocce). Conta il tempo di cottura del sugo a partire da ora.

Adesso metti l'acqua a bollire; quando l'acqua della pasta bolle il sugo dovrebbe essere quasi pronto (ovvero: sommando il tempo di cottura della pasta, assicurati che il sugo rimanga in cottura almeno 40 minuti); sala l'acqua e butta la pasta.

A cottura ultimata, scola e salta nel sugo; se ce l'hai, mettici del prezzemolo tritato, altrimenti anche del pepe macinato, se ti va.
