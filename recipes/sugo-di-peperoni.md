---
title: Sugo di peperoni
excerpt: con acciughina
publicationDate: 2022-07-17
authors:
  - T
cover:
  coverUrl: /ricettario/uploads/sugo-peperoni.webp
card:
  difficulty: facile
  servings: 4 persone
  procedureDuration: 30m
  cookingDuration: almeno 30m
tags:
  - primi
  - pasta
  - verdure
intro: Andiamo al sodo, dai.
ingredients:
  - amount: 1
    name: peperone
  - amount: 1
    name: spicchio d'aglio
  - amount: 1
    name: acciuga
  - amount: 350
    name: passata di pomodoro
    unit: ml
  - amount: 0
    name: olio EVO
  - amount: 0
    name: sale
tools:
  - padella (per il sugo)
  - pentola (per la pasta)
preservation: |-
  Il sugo si può congelare e conservare per diverse settimane.

  Se invece si tiene in frigo, consumare entro pochi giorni.
---
Aglio su filo d'olio in padella, aggiungi acciuga e lascia sciogliere.

Poi metti il peperone tagliato a listarelle e sala; sfuma con vino bianco.

Aggiungi passata e un bicchiere scarso d'acqua del rubinetto; sala a piacere.

Nel frattempo metti l'acqua per la pasta a bollire.

Tieni il sugo a cuocere per almeno 30 minuti, chiuso con coperchio, e aggiungi eventualmente acqua della pasta quando rischia di bruciare.
