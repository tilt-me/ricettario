---
title: Insalata mela verde e noci
publicationDate: 2024-01-13
authors:
  - T
card:
  servings: 2 persone
  cookingDuration: "0"
  difficulty: facile
  procedureDuration: 20 minuti
tags:
  - piatti unici
ingredients:
  - amount: 2
    name: gambi di sedano bianco
  - amount: 2
    name: mele verdi
  - amount: 12
    name: noci
  - amount: 6
    name: pomodorini secchi
  - amount: 0
    name: olio
  - amount: 0
    name: sale
  - amount: 0
    name: pepe
  - amount: 0
    name: parmigiano
  - amount: 0
    name: ½ limone
tools:
  - schiaccianoci
  - grattugia
---
Emulsione di olio, sale, pepe e il succo di mezzo limone.
