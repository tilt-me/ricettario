---
title: Polpette al forno
publicationDate: 2023-06-02
revisionDate: 2024-06-11
sources:
  - sourceUrl: https://ricette.giallozafferano.it/Polpette-al-forno.html
    sourceName: Giallo Zafferano
authors:
  - T
cover:
  coverUrl: ""
card:
  servings: 4 porzioni
  cookingDuration: 30 minuti
  difficulty: media
tags:
  - secondi
  - uova
  - carne
ingredients:
  - amount: 500
    name: macinato di manzo e/o vitello
    unit: g
  - amount: 1
    name: uovo
  - amount: 100
    name: pan grattato
    unit: g
  - amount: 50
    name: parmigiano grattugiato
    unit: g
  - amount: 0
    name: sale
  - amount: 0
    name: pepe
  - amount: 0
    name: aglio tritato
  - amount: 0
    name: prezzemolo (o rosmarino, timo)
  - amount: 0
    name: olio extravergine di oliva
tools:
  - ciotola (tappo viola)
  - pyrex grande squadrato
preservation: |
  Le polpette si conservano in frigo per un paio di giorni: falle fuori!
---
In ciotola (tappo viola) metti macinato, pan grattato, parmigiano, aglio in polvere e prezzemolo; aggiungi poi l'uovo, il sale e il pepe e amalgama il tutto col cucchiaio celeste.
Riponi la ciotola in frigo, chiusa col tappo, per almeno mezz'ora.

Fai le polpette con le mani (dovrebbero venirne 16) e mettile nel pyrex grande squadrato con due cucchiai d'olio.
Accompagna con un contorno (patate?).

Forno ventilato preriscaldato a 180° per 30 minuti. 
