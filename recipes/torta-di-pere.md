---
title: Torta di pere
excerpt: senza burro!
publicationDate: 2024-03-05
revisionDate: 2024-05-31
sources:
  - sourceName: Giallo Zafferano
    sourceUrl: https://ricette.giallozafferano.it/Torta-di-pere.html
  - sourceName: Cucchiaio d'Argento
    sourceUrl: https://www.cucchiaio.it/ricetta/ricetta-torta-pere/
authors:
  - T
card:
  procedureDuration: 1 ora
  cookingDuration: 1 ora
  difficulty: media
  servings: 8 porzioni
tags:
  - dolci
  - uova
  - colazione
ingredients:
  - amount: 700
    name: pere conference
    unit: g
  - amount: 0
    name: ½ limone non trattato
  - amount: 10
    name: rum
    unit: g
  - amount: 100
    name: zucchero bianco
    unit: g
  - amount: 2
    name: uova
  - amount: 150
    name: farina di riso
    unit: g
  - amount: 50
    name: fecola di patate
    unit: g
  - amount: 25
    name: amido di mais
    unit: g
  - amount: 0
    name: zucchero di canna
  - amount: 12
    name: lievito per dolci
    unit: g
  - amount: 100
    name: latte intero
    unit: ml
tools:
  - ciotola in vetro media (tappo viola)
  - pelapatate
  - bilancia elettronica
  - ciotola di vetro quadrata bassa
  - tortiera tonda antiaderente (diametro 22cm)
  - carta forno
  - mini grattugia
  - setaccio
  - fruste elettriche
  - ciotola beige grande
  - stuzzicadenti
  - forno elettrico
  - zuppiera rossa (per conservarla)
---
Lava il mezzo limone e grattugiane la buccia (molto superficialmente, senza arrivare al bianco ma rimanendo sul giallo); raccogli la buccia in una ciotolina e poi spremine il succo in un bicchiere; aggiungi al bicchiere anche il rum.

Passa alle pere: sbucciale col pelapatate, dividile a metà, rimuovi il torsolo e tagliale a fette sottili; raccoglile quindi nella ciotola di vetro aggiungendo la bagna di succo di limone e rum.

Non volendo usare burro, fodera la tortiera con la carta forno; oppure scendi ad un compromesso e mettilo almeno sul fondo della teglia, spolverandoci sopra un po' di farina.

Con la bilancia elettronica pesa le polveri aggiungendole una alla volta nella ciotola di vetro quadrata (per seguire la maniera fricchettona di sostituire la farina 00, nota le proporzioni: 6/9 di farina di riso, 2/9 di fecola e 1/9 di amido).

Comincia a preriscaldare il forno a 180°.

Metti nella ciotola beige lo zucchero bianco e le uova, quindi monta con le fruste elettriche; aggiungi poi le polveri, setacciandole a poco a poco, e il limone grattugiato. Quando il composto è bello compatto, aggiungi un po' di latte, ma giusto lo stretto necessario per renderlo pastoso e plastico; aggiungi quindi le pere, avendo cura di scolarle il più possibile prima di immergerle nell'impasto (se è troppo liquido, poi la torta non lievita). Versa tutto nella tortiera e spolvera la superficie con un leggerissimo strato di zucchero di canna.

Inforna per mezz'ora impostando la modalità statica, e poi (senza aprire lo sportello, ricorda nonna papera!) passa alla modalità ventilato per un'altra mezz'ora. Dopo un'ora verifica la cottura con uno stuzzicadenti e in caso positivo lasciala raffreddare prima di rimuoverla dallo stampo.

Per servirla puoi spolverare le fette con zucchero a velo e/o cannella.
