---
title: Caffellatte fricchettone
excerpt: Senza caffè, senza latte!
publicationDate: 2023-11-11
authors: ""
cover:
  coverUrl: /ricettario/uploads/caffellatte.avif
card:
  servings: 1 persona
  procedureDuration: 1 minuto
  cookingDuration: 5 minuti
  difficulty: facile
tags:
  - bevande
ingredients:
  - amount: 0
    name: 1 tazza di bevanda di riso o mandorla
  - amount: 1
    name: cucchiaino bello pieno di orzo solubile
tools:
  - pentolino scaldalatte
---
Scalda la bevanda in un pentolino, senza portare ad ebollizione (perde proprietà, cazzi e mazzi).

Metti l'orzo nella tazza, versaci la bevanda calda e mescola, avendo cura di far sciogliere gli eventuali grumi schiacciandoli col cucchiaino sul bordo della tazza.

Non aggiungere zucchero (è già presente nella bevanda di cui sopra).
