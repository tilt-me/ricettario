---
title: Pan banana
excerpt: detto anche banana bread
publicationDate: 2023-06-08
sources:
  - sourceName: Fatto in casa da Benedetta
    sourceUrl: https://www.fattoincasadabenedetta.it/pan-banane-banana-bread/
  - sourceUrl: https://lacucinadibimbapimba.blogspot.com/2019/04/conversione-farina-00-per-dolci-semplici.html
    sourceName: Conversione farina 00
  - sourceUrl: https://clibi.net/2016/07/05/come-sostituire-lo-xantano/
    sourceName: Come sostituire lo xantano
authors:
  - T
cover:
  coverUrl: /ricettario/uploads/pan-banana.webp
card:
  cookingDuration: 40 minuti
  difficulty: facile
tags:
  - dolci
  - uova
  - colazione
ingredients:
  - amount: 166.6
    name: farina di riso
    unit: g
  - amount: 55.5
    name: fecola di patate
    unit: g
  - amount: 27.7
    name: amido di mais
    unit: g
  - amount: 6.3
    name: agar agar
    unit: g
  - amount: 8
    name: lievito per dolci
    unit: g
  - amount: 8
    name: bicarbonato
    unit: g
  - amount: 150
    name: zucchero bianco
    unit: g
  - amount: 8
    name: cannella (opzionale)
    unit: g
  - amount: 2
    name: banane mature
  - amount: 2
    name: uova
  - amount: 60
    name: olio di semi di arachidi
    unit: g
  - amount: 50
    name: acqua
    unit: g
tools:
  - bilancia (precisa al decimo di grammo)
  - frullatore a immersione
  - frusta (accessorio del frullatore)
  - recipiente del frullatore (tarato)
  - recipiente tarato in plastica
  - ciotola beige grande
  - stampo per plumcake (in silicone)
  - forno elettrico
  - stuzzicadenti
preservation: >
  Il pan banana si conserva per 3-4 giorni a temperatura ambiente, coperto dalla
  ciotola rossa.
relatedRecipe:
  - panna-montata
---
Nel recipiente del frullatore ad immersione (che è tarato) metti l'olio di semi, l'acqua e le banane a fette; frulla tutto. Poi aggiungi anche le uova, e sbatti con la frusta (cambiando l'accessorio).

In ciotola grande (beige con manico) metti le polveri (conteggiate col recipiente tarato in plastica) e frusta tutto, aggiungendo il composto di banane.

Versa tutto nello stampo per plumcake in silicone. Forno ventilato preriscaldato a 180° per 40 minuti, poi controlla con stuzzicadenti; se è ancora umido ma comincia a bruciarsi la crosta, spostalo al ripiano più basso, coprilo con stagnola, ma continua ancora 10-20 minuti.

Accostamenti: zucchero a velo, panna montata, crema di nocciole spalmabile, yogurt, tè.
