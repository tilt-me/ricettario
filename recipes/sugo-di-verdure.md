---
title: Sugo di verdure
excerpt: ""
publicationDate: 2024-02-16
authors:
  - T
cover:
  coverUrl: ""
card:
  difficulty: facile
  servings: ""
  procedureDuration: 30m
  cookingDuration: almeno 30m
tags:
  - primi
  - pasta
  - verdure
intro: Andiamo al sodo, dai.
ingredients:
  - amount: 0
    name: Melanzane o Zucchine o Funghi
  - amount: 0
    name: olio EVO
  - amount: 1
    name: spicchio d'aglio
  - amount: 0
    name: ½ carota
  - amount: 0
    name: ½ cipolla
  - amount: 1
    name: gambo di sedano
  - amount: 700
    name: passata di pomodoro
    unit: ml
  - amount: 0
    name: alloro o basilico
  - amount: 1
    name: cucchiaio di vino bianco
  - amount: 80
    name: di pasta a testa
    unit: g
  - amount: 0
    name: sale
tools:
  - tritatutto
  - padella alta (per contenere tutto il barattolo di sugo)
  - pentola (per bollire la pasta)
  - scolapasta
preservation: |
  Il sugo si può congelare e conservare per diverse settimane.

  Se invece lo tieni in frigo, consumalo entro pochi giorni.
---
Comincia tagliando a cubetti la verdura scelta; mettila da parte.

Prepara il soffritto mettendo lo spicchio d'aglio su un filo d'olio in padella e poi aggiungi trito di cipolla, carota e sedano (fatto col tritatutto).

Quando il soffritto è pronto (prima che cominci a bruciare) aggiungi la verdura a cubetti e sala.

Quindi sfuma con vino bianco e aggiungi la passata, la foglia di alloro e un bicchiere scarso di acqua del rubinetto (puoi usare la bottiglia appena svuotata della passata); sala a piacere.

Adesso metti l'acqua a bollire per la pasta.

Tieni il sugo a cuocere per almeno 30 minuti, chiuso con coperchio, e aggiungi eventualmente acqua della pasta quando rischia di bruciare.
